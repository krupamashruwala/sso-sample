package com.vmware.nee.saml;

import org.opensaml.common.SAMLException;
import org.opensaml.common.SAMLRuntimeException;
import org.opensaml.xml.encryption.DecryptionException;
import org.opensaml.xml.security.SecurityException;
import org.opensaml.xml.validation.ValidationException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.providers.ExpiringUsernameAuthenticationToken;
import org.springframework.security.saml.SAMLAuthenticationProvider;
import org.springframework.security.saml.SAMLAuthenticationToken;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.security.saml.context.SAMLMessageContext;

import java.util.Collection;
import java.util.Date;

/**
 * A {@link org.springframework.security.saml.SAMLAuthenticationProvider} subclass to return 
 * principal as UserDetails Object. 
 * 
 */
public class GrailsSAMLAuthenticationProvider extends SAMLAuthenticationProvider {
	public GrailsSAMLAuthenticationProvider() {
		super();
	}
	
	/**
     * @param credential credential used to authenticate user
     * @param userDetail loaded user details, can be null
     * @return principal to store inside Authentication object
     */
	@Override
    protected Object getPrincipal(SAMLCredential credential, Object userDetail) {
		if (userDetail != null) {
			return userDetail;
	}

		return credential.getNameID().getValue();
}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		if (!this.supports(authentication.getClass())) {
			throw new IllegalArgumentException("Only SAMLAuthenticationToken is supported, " + authentication.getClass() + " was attempted");
		} else {
			SAMLAuthenticationToken token = (SAMLAuthenticationToken)authentication;
			SAMLMessageContext context = token.getCredentials();
			SAMLCredential credential;

			try {
				if ("urn:oasis:names:tc:SAML:2.0:profiles:SSO:browser".equals(context.getCommunicationProfileId())) {
					credential = this.consumer.processAuthenticationResponse(context);

				} else {
					if (!"urn:oasis:names:tc:SAML:2.0:profiles:holder-of-key:SSO:browser".equals(context.getCommunicationProfileId())) {
						throw new SAMLException("Unsupported profile encountered in the context " + context.getCommunicationProfileId());
					}
					credential = this.hokConsumer.processAuthenticationResponse(context);
				}
			} catch (SAMLRuntimeException var10) {
				this.samlLogger.log("AuthNResponse", "FAILURE", context, var10);
				throw new AuthenticationServiceException("Error validating SAML message", var10);
			} catch (SAMLException var11) {
				var11.printStackTrace();
				this.samlLogger.log("AuthNResponse", "FAILURE", context, var11);
				throw new AuthenticationServiceException("Error validating SAML message", var11);
			} catch (ValidationException var12) {
				this.samlLogger.log("AuthNResponse", "FAILURE", context);
				throw new AuthenticationServiceException("Error validating SAML message signature", var12);
			} catch (SecurityException var13) {
				this.samlLogger.log("AuthNResponse", "FAILURE", context);
				throw new AuthenticationServiceException("Error validating SAML message signature", var13);
			} catch (DecryptionException var14) {
				this.samlLogger.log("AuthNResponse", "FAILURE", context);
				throw new AuthenticationServiceException("Error decrypting SAML message", var14);
			}

			Object userDetails = this.getUserDetails(credential);
			Object principal = this.getPrincipal(credential, userDetails);
			Collection<? extends GrantedAuthority> entitlements = this.getEntitlements(credential, userDetails);
			Date expiration = this.getExpirationDate(credential);
			ExpiringUsernameAuthenticationToken result = new ExpiringUsernameAuthenticationToken(expiration, principal, credential, entitlements);
			result.setDetails(userDetails);
			this.samlLogger.log("AuthNResponse", "SUCCESS", context, result, (Exception)null);
			return result;
		}
	}
}