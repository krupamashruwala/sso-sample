package au.com.school;

class SchoolUser extends User {

    SchoolUser createdBy
    String pwdResetKey
    Date pwdResetReqDt
    String passcode
    Boolean passcodeResetRequired = true
    Boolean passwordResetRequired = true
    UserType userType
    String userDeviceId
    Integer userId
    String contactType
    String relationShipToStudent


    static constraints = {
        userType nullable: true
        createdBy nullable: true
        pwdResetKey nullable: true
        pwdResetReqDt nullable: true
        passcode nullable: true
        passcodeResetRequired nullable: true
        passwordResetRequired nullable: true
        userDeviceId nullable: true
        userId nullable: true
        password nullable: true
        contactType nullable: true
        relationShipToStudent nullable: true
    }

    enum UserType {
        ADMIN, STAFF, PARENT
    }

    def beforeInsert() {
        super.beforeInsert()
        createdBy = springSecurityService?.currentUser

        //updateUserType()
    }

    def beforeUpdate() {
        super.beforeUpdate()

        updateUserType()
        createRole()
    }

    def updateUserType() {
       /* if (this.userType == UserType.ADMIN) {
            this.type = au.com.school.User.UserType.ADMIN

        } else {
            this.type = au.com.school.User.UserType.USER
        }*/
    }

    def createRole() {
        UserRole.withNewSession {
            def role = null

            UserRole.removeAll(this)

            if (this.userType == UserType.PARENT) {
                role = Role.findByAuthority('ROLE_PARENT')

            } else if (this.userType == UserType.STAFF) {
                role = Role.findByAuthority('ROLE_STAFF')

            } else if (this.userType == UserType.ADMIN) {
                role = Role.findByAuthority('ROLE_ADMIN')
            }

            if (role) {
                new UserRole(user: SchoolUser.get(id), role: role).save flush: true
            }
        }

    }

    public String toString() {
        return firstName + " " + lastName + " - "
    }
}
