package au.com.school

class User {

	transient springSecurityService

	String username
	String password
	String firstName
	String lastName
	String email
	String ssoToken

	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	Date dateCreated
	Date lastUpdated

	Boolean deleted = false

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false
		firstName nullable: true
		ssoToken nullable: true
		lastName nullable: true
		email nullable: true, email: true, blank: false
	}

	static mapping = {
		password column: '`password`'
	}

	enum UserType {
		MOBI_ADMIN, SUPER_ADMIN, ADMIN, SOCIAL, DEVELOPER, MANAGER, USER, INCIDENT_ADMIN, INCIDENT_USER, MERCHANT, THIRD_PARTY
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByAccount(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}