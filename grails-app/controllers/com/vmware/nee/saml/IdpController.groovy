package com.vmware.nee.saml

import au.com.school.User
import grails.converters.JSON
import org.opensaml.saml2.metadata.provider.FilesystemMetadataProvider
import org.opensaml.saml2.metadata.provider.MetadataProvider
import org.opensaml.xml.parse.BasicParserPool
import org.springframework.security.saml.metadata.ExtendedMetadata
import org.springframework.security.saml.metadata.ExtendedMetadataDelegate

class IdpController {

	def metadata
	def springSecurityService

    def index() {
		render view:"index"
	}

	def ssoAssertionFail() {
		def error = [success: false, message: "user is not authenticated"]
	}

	def sendSsoAssertionToMobiApp() {
		def data = [:]

		if(params.success == "true") {
			data = [success: Boolean.TRUE]
			render data as JSON

		} else {
			data = [success: Boolean.FALSE]
			render data as JSON
		}
	}

	def ssoAssertionToMobiApp() {
		def userName = springSecurityService.authentication.name

		User user = User.findByUsername(userName)
		def userData
			if(user) {
			redirect(action:"sendSsoAssertionToMobiApp", params:[success: Boolean.TRUE, userName: userName, password: user.ssoToken, ssoToken: user.ssoToken])
		} else {
			redirect(action:"sendSsoAssertionToMobiApp", params:[success: Boolean.FALSE, message: "user is not authenticated"])
		}
	}

	def add(params){
		def filename = params?.file?.trim()
		if(filename){
			def file = new File(filename)
			FilesystemMetadataProvider provider = new FilesystemMetadataProvider(file)
			provider.setParserPool(new BasicParserPool())
			provider.initialize()
			MetadataProvider metadataProvider = new ExtendedMetadataDelegate(provider)
			metadata.addMetadataProvider(metadataProvider)
			metadata.setRefreshRequired(true)
			metadata.refreshMetadata()
		}
		redirect action: "list"
	}
	
	def remove(params){
		def entityName = params?.idp?.trim()
		if(entityName){
			metadata.providers.each{
				def name = metadata.parseProvider(it)[0] //if there is multiple entities in provider..This may not be right
				if(name == entityName) metadata.removeMetadataProvider(it)
				metadata.refreshMetadata()
			}
		}
		redirect action: "list"
	}
	
	def list(){
		render view: "list"
	}
}