import com.vmware.nee.saml.IdpController

class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.${format})?"{
            constraints {
                // apply constraints here
            }
        }

        "/ssoAssertion"(controller: "idp", action: "ssoAssertionToMobiApp")
        "/ssoAssertion/fail"(controller: "idp", action: "ssoAssertionFail")

        "/"(view: "index")

        "500"(view:'/error')
	}
}
