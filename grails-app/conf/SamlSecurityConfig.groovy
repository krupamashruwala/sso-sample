security {

    // authenticationFailureHandler
    failureHandler {
        defaultFailureUrl = '/ssoAssertion/fail'
    }

	saml {
		userAttributeMappings = ['firstName': 'FirstName', 'lastName': 'LastName', 'email': 'EmailAddress']
		userGroupToRoleMapping = [:]
		active = true
		afterLoginUrl = '/ssoAssertion'
		afterLogoutUrl = '/'
		loginFormUrl = '/saml/login'
		userGroupAttribute = "memberOf"
		responseSkew = 60
		idpSelectionPath = '/'
		autoCreate {
			active =  true
			key = 'username'
			assignAuthorities = true
		}

		metadata {
			defaultIdp = 'https://cggs-login.cloudworkengine.net/saml2/idp/metadata.php'
			url = '/saml/metadata'
			//default idp info
			idp{
				file = 'security/idp.xml'
				alias = 'https://cggs-login.cloudworkengine.net/saml2/idp/metadata.php'
			}
			sp {

				file = 'security/sp.xml'
				alias = 'mobbidiction_sp_007'
				defaults{
					local = true
					alias = 'mobbidiction_sp_007'
					signingKey = 'ping'
					encryptionKey = 'ping'
					tlsKey = 'ping'
					requireArtifactResolveSigned = false
					requireLogoutRequestSigned = false
					requireLogoutResponseSigned = false
					idpDiscoveryEnabled = true
				}
			}
		}
		keyManager {
			storeFile = 'classpath:security/keystore.jks'
			storePass = 'nalle123'
			passwords = [ ping: 'ping123' ]
			defaultKey = 'ping'
		}
	}
}